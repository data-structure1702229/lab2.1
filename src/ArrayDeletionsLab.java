public class ArrayDeletionsLab {

    public static int[] deleteElementByIndex(int[] arr, int index){
        
        int [] newarr =new int[arr.length-1];
        for(int i=0;i<index;i++){
            newarr[i]=arr[i];

        }
        for(int i=index+1;i<arr.length;i++){
            newarr[i-1]=arr[i];
        }
        return newarr;


    }//[1,2,4,5]
    public static int[] deleteElementByValue(int[] arr, int value){
        int [] newarr =new int[arr.length-1];
         for(int i=0;i<arr.length ;i++){
            
            if(arr[i]==value){
                for(int j=0;j<i ;j++){
                    newarr[j]=arr[j];
                }
                for(int j=i+1;j<arr.length;j++){
                        newarr[j-1]=arr[j];
                        }
            }
            
        }
        
        return newarr;

    }
    
    public static void printArray(int[] array){
        System.out.print("[");
        for(int i=0; i<array.length;i++){
            System.out.print(array[i]);
            if(i<array.length-1){
            System.out.print(",");
            }
        }
        System.out.println("]");
    }



    public static void main(String[] args)  {
        int[] arr  ={1, 2, 3, 4, 5} ;
        int index =2;
        int value =4;
        System.out.print("Original Array: ");
        printArray(arr);


        arr=deleteElementByIndex(arr, index);
        System.out.print("Array after deleting element at index "+index+": ");
        printArray(arr);
        arr= deleteElementByValue(arr, value);
        System.out.print("Array after deleting element with value "+value+": ");
        printArray(arr);

        
       
    }
}
